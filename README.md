# Stressman 🙀

The world's worst load tester 

## Installation 
### Global install

You will need a NPM_TOKEN set and a .npmrc in your home directory or project folder. The content of .npmrc is ```//registry.npmjs.org/:_authToken=${NPM_TOKEN}```

```
$ npm install -g @dahl-js/stressman
$ stressman [options] <file> [report-filename]
```

### Local install

```
$ git clone https://bitbucket.org/slopeflax/stressman.git
$ cd stressman
$ npm run stressman <file> [report-filename] -- [options]
```

## Running

**NOTE!** If you get a certificate error set environment variable NODE_TLS_REJECT_UNAUTHORIZED=0

There are two execute options for a request, X amount of times (-t N) or for X seconds (-s N). 
Each request can then be executed in parallel (-p N)

### Request file format
Examples exists in [requests](requests) folder

```
{
  "url": "https://qa-api.dahl.se/product-information-service/v1/menu", // REQUIRED
  "maxTime": 100, // OPTIONAL - execution is considered a failure if response takes longer (ms)
  "method": "GET", // REQUIRED
  "headers": {
    "Content-Type": "application/json" // OPTIONAL
  }
}
```
#### Create request file
```
$ stressman --seed myrequest.json
```

### Environment variables

`STRESSMAN_AUTH_HEADER` - add/override Authorization header to all requests
`STRESSMAN_KEEP_ALIVE` - reuse https sockets

### Examples
Space between option and value is optional

#### Display help:
```
$ stressman -h
```

#### Run two request in parallel two times:

```
$ stressman -t2 -p2 request.json
```
#### Run ten requests in parallel for 60 seconds with a 100 ms sleep between each set of ten.

```
$ stressman -s60 -p10 -r100 request.json
```

#### Run one request and create a report in json format
Report file name will be request_report.json
```
$ stressman request.json report.json
```

#### Run one request and create a report in csv format
Report file name will be request_report.csv
```
$ stressman request.json report.csv
```

#### Run all requests in a folder

```
$ stressman request.json my-request-suite
```

#### Run one request and exit with error if average response time is greater than 100 ms
Running command with a failing expectation will result in exit code 1, so if it's executed in 
a Bitbucket pipeline will result in failure.

Setting `maxTime` in the request.json takes precedence.

```
$ stressman -e100 request.json
```

