#!/usr/bin/env node
import fs from 'fs'
import path from 'path'
import { program } from 'commander'
import { Runner } from './runner.js'
import { logError, logInfo } from './console.js'

const parseFile = (fileName) => ({ file: fileName, data: JSON.parse(fs.readFileSync(fileName)) })

program.version('1.9.0')
program
  .option('-t, --times <times>', 'run given executions')
  .option('-s, --seconds <seconds>', 'execute for given seconds')
  .option('-p, --parallel <requests>', 'how many parallel requests?')
  .option('-r, --relax <ms>', 'sleep time between executions')
  .option('-e, --expect <ms>', 'expected average request time in ms')
  .option('--seed', 'create request skeleton file')
  .showHelpAfterError(true)
  .argument('<file>', 'request file or folder')
  .argument('[report]', 'report name (or suffix if source is a folder)')
  .action(async (file, reportName) => {
    const options = program.opts()

    if (options.seed) {
      fs.writeFileSync(file, JSON.stringify({ url: '<url>', method: 'POST, GET etc', maxTime: 10000, headers: { 'Optional-Header-Name': 'Optional-Value' }, body: { requiredIfPost: true } }))
      return
    }

    if (!fs.existsSync(file)) {
      logError(`Path not found: ${file}`)
    }

    const requests = fs.lstatSync(file).isDirectory()
      ? fs.readdirSync(file).map(f => parseFile(path.join(file, f)))
      : [parseFile(file)]

    for (const request of requests) {
      logInfo(`# Execute ${request.file}\n`)
      if (process.env.STRESSMAN_AUTH_HEADER) {
        request.data.headers = {
          ...request.data.headers,
          Authorization: process.env.STRESSMAN_AUTH_HEADER
        }
      }
      const runner = new Runner(options)
      try {
        await runner.run(request, reportName)
      } catch (error) {
        logError(error.message)
        process.exitCode = 1
      } finally {
        console.log('\n')
      }
    }
  })
  .parse(process.argv)
