import got from 'got'
import fs from 'fs'
import Agent from 'agentkeepalive'
import { logHeader, logSuccess, logError } from './console.js'

const sleep = (milliseconds) => {
  if (milliseconds) {
    console.log(`🛌 ${milliseconds} ms`)
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  } else {
    return Promise.resolve()
  }
}

const TABLE_HEAD = ['Batch', 'Status', 'Wait', 'DNS', 'TCP', 'TLS', 'Req', 'TTFB', 'DL', 'Total', 'Expected']

const keepaliveAgent = new Agent.HttpsAgent({
  rejectUnauthorized: process.env.NODE_TLS_REJECT_UNAUTHORIZED === '0',
  ciphers: 'AES256-GCM-SHA384',
  timeout: 60000,
  freeSocketTimeout: 30000
})

export class Runner {
  constructor (options) {
    this.options = options
    this.report = {
      avg: 0,
      pass: true,
      expect: options?.expect ? parseInt(options.expect) : Number.MAX_SAFE_INTEGER,
      requests: []
    }
  }

  async makeRequest (msg, request) {
    const { url, method, headers, body } = request
    return got(url, {
      method,
      body: JSON.stringify(body),
      headers,
      agent: {
        https: process.env.STRESSMAN_KEEP_ALIVE === 'true' ? keepaliveAgent : undefined
      }
    })
      .then(response => {
        const phases = response.timings.phases
        const { wait, dns, tcp, tls, request, firstByte, download, total } = phases

        logSuccess([msg, response.statusCode, wait, dns, tcp, tls, request, firstByte, download, total, this.options.expect].join('\t'))
        this.report.requests.push({ batch: msg, status: response.statusCode, url, headers: response.headers, ...response.timings.phases })
      }
      )
      // .then(response => console.debug({ batch: msg, status: response.statusCode, type: 'performance', url, headers: response.headers, duration: response.timings.phases.total, timings: response.timings.phases }))
  }

  createPromises (total, batch, request) {
    const promises = []

    for (let i = 0; i < total; i++) {
      promises.push(this.makeRequest(`${i}/${batch}`, request))
    }

    return promises
  }

  async run (request, reportName) {
    logHeader(TABLE_HEAD.join('\t'))

    const { seconds, parallel } = this.options

    if (seconds !== undefined) {
      const until = new Date().getTime() + (seconds * 1000)
      while (new Date().getTime() < until) {
        const promises = this.createPromises(parallel, 1, request.data)

        await Promise.all(promises)
        await sleep(this.options.relax)
      }
    } else {
      const times = this.options.times || 1
      const parallel = this.options.parallel || 1

      for (let i = 0; i < times; i++) {
        const promises = this.createPromises(parallel, i, request.data)

        await Promise.all(promises)
        await sleep(this.options.relax)
      }
    }

    const expect = request.maxTime || this.options.expect
    const total = this.report.requests.map(entry => entry.total).reduce((partialSum, acc) => partialSum + acc, 0)
    this.report.avg = Math.ceil(total / this.report.requests.length)

    if (reportName !== undefined) {
      const reportFileName = request.file.substring(request.file.lastIndexOf('/') + 1, request.file.indexOf('.')) + '_' + reportName

      if (reportFileName.endsWith('.json')) {
        fs.writeFileSync(reportFileName, JSON.stringify(this.report))
      } else if (reportFileName.endsWith('.csv')) {
        const csv = fs.createWriteStream(reportFileName, { flags: 'w' })
        csv.write(`${TABLE_HEAD.join(',')}\n`)
        for (const row of this.report.requests) {
          csv.write([row.batch, row.status, row.wait, row.dns, row.tcp, row.tls, row.request, row.firstByte, row.download, row.total, this.options.expect].join(','))
          csv.write('\n')
        }
        csv.close()
      } else {
        logError('I don\'t understand the output format for the report, only .json or .csv')
      }
    }

    if (expect !== undefined && this.report.avg > parseInt(expect)) {
      this.report.pass = false
      throw new Error(`Failed to execute in a timely fashion. Expected ${expect}, got ${this.report.avg}`)
    }
  }
}
